<?php

namespace App\Container;

class Container extends \Components\Container\Container
{
    public function getRouter()
    {
        return $this->get('router');
    }

    /**
     * @return \Components\Request
     */
    public function getRequest()
    {
        return $this->get('request');
    }

    public function getTemplate()
    {
        return $this->get('template');
    }

    /**
     * @return \Components\UserToken
     */
    public function getUserToken()
    {
        return $this->get('user_token');
    }

    /**
     * @return \Components\DB\DB
     */
    public function getDB()
    {
        return $this->get('db');
    }

    /**
     * @return \Components\DB\ObjectManager
     */
    public function getORM()
    {
        return $this->get('orm');
    }

    public function getUserRepository()
    {
        return $this->getORM()->getRepository('App\\Entity\\User');
    }
}
