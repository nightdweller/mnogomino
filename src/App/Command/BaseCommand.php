<?php

namespace App\Command;

use App\Container\Container;
use Components\Console\ArgvInput;
use Components\Console\CommandInterface;

class BaseCommand implements CommandInterface
{
    /**
     * @var Container $container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function execute(ArgvInput $input)
    {

    }

    public function getName()
    {
        return '';
    }
}
