<?php

namespace App\Command;

use Components\Console\ArgvInput;

class TestCommand extends BaseCommand
{
    public function execute(ArgvInput $input)
    {
        $time = microtime(true);
        for ($i = 0; $i < 5000; $i++) {
            file_get_contents('http://127.0.0.1/mnogomino/web/index.php/main?viewer_id=4');
        }

        var_dump(microtime(true) - $time);
    }

    public function getName()
    {
        return 'test';
    }
}
