<?php

namespace App\Entity;

class User
{
    protected $id;
    protected $vkId;
    protected $level;
    protected $lastUpdate;

    public function getId()
    {
        return $this->id;
    }

    public function getVkId()
    {
        return $this->vkId;
    }

    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }
}