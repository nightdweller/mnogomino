<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="<?= $this->fileAsset('js/jquery.min.js') ?>"></script>
    <script src="<?= $this->fileAsset('js/jquery-ui.min.js') ?>"></script>

    <link href='//fonts.googleapis.com/css?family=Roboto:300,500,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="<?= $this->fileAsset('css/main.css') ?>" />

    <script src="//vk.com/js/api/xd_connection.js?2"></script>
    <script src="//vk.com/js/api/openapi.js?116"></script>

    <script src="//ad.mail.ru/static/vkadman.min.js" type="text/javascript"></script>
    <script src="//js.appscentrum.com/scr/preroll.js" type="text/javascript"></script>

    <script src="<?= $this->fileAsset('js/app.js') ?>"></script>
    <script src="<?= $this->fileAsset('js/helper.js') ?>"></script>
    <script src="<?= $this->fileAsset('js/user.js') ?>"></script>
    <script src="<?= $this->fileAsset('js/game.js') ?>"></script>
</head>
<body>


<div class="back js-yd-black"></div>
<div class="message-row" style="width: 100%; text-align: center; position: absolute; z-index: 999;">
    <div class="message box-shadow main-msg js-yd-msg" style="{% block message_style %}display: inline-block; position: static; top: 11px;{% endblock %}">
        <div style="font-size: 40px; font-weight: 900;" class="js-yd-msg-header">
        </div>
        <div style="padding-bottom: 25px; padding-top: 10px; padding-left: 10px; padding-right: 10px; font-size: 20px; font-weight: 300;" class="js-yd-msg-content">
        </div>

        <a href="#" onclick="App.closeMessage();" class="btn btn-red font-medium js-yd-msg-close">
            ПРОДОЛЖИТЬ
        </a>
    </div>
</div>

<div class="container js-container">
    <?= function_exists('content') ? content() : '' ?>
</div>


<script>
    App.isFirst = false;
//    App.apiUrl = '<?//= $this->url() ?>//'
//    App.apiUrl = '{{ url('ajax', {type: game.alias, siteName: site.alias}) }}';

    User = {};
</script>

<script>
    App.page('<?= $this->url('main') ?>');
</script>

</body>

</html>
