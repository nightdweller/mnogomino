<?php

namespace App\Controller;

use Components\Request;

class GameController extends BaseController
{
    public function mainAction(Request $request)
    {
        return $this->render('Game/main.php');
    }

    public function gameAction(Request $request)
    {
        return $this->render('Game/game.php');
    }

    public function indexAction(Request $request)
    {
        return $this->render('layout.php', [
            'word' => 'hello'
        ]);
    }
}