<?php

namespace Components;

class Response
{
    protected $content;

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
    }

    public function sendHeaders()
    {
        header('Content-Type: text/html; charset=utf-8');
    }

    public function sendContent()
    {
        echo $this->content;

        return $this;
    }
}