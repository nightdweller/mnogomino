<?php

namespace Components\PhpTemplate;

use Components\Container\Container;
use Components\Container\ContainerInterface;

class Template
{
    private $container;
    private $view = null;
    private $params;
    private $dir;
    private $basePath;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->basePath = $_SERVER['BASE'];
    }

    public function render($path, $view, $parameters = [])
    {
        $this->dir = $path;
        $this->params = $parameters;

        $this->view = $view;

        ob_start();
        while ($this->view) {
            $view = $this->view;
            $this->view = null;

            include($path . DIRECTORY_SEPARATOR . $view);
        }

        $result = ob_get_contents();
        ob_clean();

        $this->params = null;
        return $result;
    }

    public function url($path)
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $this->basePath . '/' . $path;
    }

    public function asset($path)
    {
        return $this->basePath . '/' . $path;
    }

    public function fileAsset($path)
    {
        return $this->basePath . '/' . $path;
    }

    private function extendsFrom($view)
    {
        $this->view = $view;
    }

    private function prepare($view)
    {
        return $this->dir . DIRECTORY_SEPARATOR . $view;
    }

    public function get($key)
    {
        return $this->params[$key];
    }
}