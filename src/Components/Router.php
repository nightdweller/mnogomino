<?php

namespace Components;

use Components\Container\Container;

class Router
{
    private $config;
    private $inverse = [];

    /**
     * @var Request
     */
    private $request;

    public function __construct(Container $container)
    {
        $this->request = $container->get('request');
        $this->config = require_once($container->getConfigDir() . 'routing.php');
    }

    public function getRoute()
    {
        if (array_key_exists($path = $this->request->getPath(), $this->config)) {
            return $this->config[$path];
        }

        return null;
    }
}