<?php

namespace Components;

class Request
{
    protected $path;

    public function __construct()
    {
        $this->path = mb_strcut($_SERVER['REQUEST_URI'], strlen($_SERVER['BASE']));
    }

    public function get($key) {
        if (array_key_exists($key, $_GET)) {
            return $_GET[$key];
        }

        return null;
    }

    public function post($key) {
        if (array_key_exists($key, $_POST)) {
            return $_POST[$key];
        }

        return null;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }
}