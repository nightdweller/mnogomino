<?php

namespace Components\Kernel;


class Kernel
{
    protected $container;

    public function __construct($container_class)
    {
        $this->container = new $container_class($this);
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function handleRequest()
    {
        $router = $this->container->get('router');

        if (null === $route = $router->getRoute()) {
            throw new \Exception();
        }

        list($class, $action) = $route;

        $controller = new $class($this->container);
        $response = $controller->{$action . 'Action'}($this->container->get('request'));

        return $response;
    }
}