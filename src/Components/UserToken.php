<?php

namespace Components;

class UserToken
{
    private $user;

    public function getUser()
    {
        if ($this->user) {
            return $this->user;
        }

        return null;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
}