<?php

namespace Components\Controller;

use Components\Container\ContainerInterface;
use Components\PhpTemplate\Template;
use Components\Response;

class AbstractController
{
    protected $container;
    protected $dir;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function render($view, array $parameters = [])
    {
        /** @var Template $template */
        $template = $this->container->get('php_template');

        return new Response($template->render($this->container->getViewsDir(), $view, $parameters));
    }

}