<?php

namespace Components\DB;

class DB
{
    protected $connection;

    public function __construct($host, $user, $password, $dbname)
    {
        $this->connection = new \mysqli($host, $user, $password, $dbname);
    }

    /**
     * @return \mysqli
     */
    public function getConnection()
    {
        return $this->connection;
    }
}