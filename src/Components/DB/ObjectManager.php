<?php

namespace Components\DB;

use Components\Container\ContainerInterface;

class ObjectManager
{
    /** @var DB */
    private $db;
    private $container;

    private $repository = [];
    private $mapping;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->db = $container->get('db');
        $this->mapping = require_once($this->container->getConfigDir() . '/mapping.php');
    }

    public function createInstance(\ReflectionClass $class, $values)
    {
        $className = $class->getName();
        $object = new $className();
        foreach ($values as $key => $value) {
            $prop = $class->getProperty($key);
            $prop->setAccessible(true);
            $prop->setValue($object, $value);
        }

        return $object;
    }

    /**
     * @param $class
     * @return Repository
     */
    public function getRepository($class)
    {
        if (array_key_exists($class, $this->repository)) {
            return $this->repository[$class];
        }

        return $this->repository[$class] = new Repository($this, $class, $this->mapping[$class]);
    }

    public function getDB()
    {
        return $this->db;
    }
}