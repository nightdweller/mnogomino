<?php

namespace Components\DB;

class Repository
{
    private $class;
    private $reflection;

    private $table;
    private $dbMapping;
    private $objectMapping;

    private $orm;
    private $connection;

    public function __construct(ObjectManager $orm, $class, array $mapping)
    {
        $this->orm = $orm;
        $this->connection = $orm->getDB()->getConnection();

        $this->class = $class;
        $this->reflection = new \ReflectionClass($class);

        $this->table = $mapping['table'];
        $this->objectMapping = $mapping['mapping'];
        $this->dbMapping = array_flip($mapping['mapping']);
        $this->fields = array_keys($this->dbMapping);
    }

    public function find($id)
    {
        return reset($this->findBy(['id' => $id]));
    }

    public function findOneBy(array $criteria)
    {
        return reset($this->findBy($criteria));
    }

    public function findBy(array $criteria)
    {
        $equations = [];
        foreach ($criteria as $key => $value) {
            $equations[] = $this->objectMapping[$key] . ($value === null ? ' IS NULL ' : ' = "' . (string)$value . '"');
        }

        $sql = 'SELECT * FROM ' . $this->table;
        if ($equations) {
            $sql .= ' WHERE ' . implode(' AND ', $equations);
        }

        return $this->fetchSQL($sql);
    }

    private function fetchSQL($sql)
    {
        $result = [];

        $query = $this->connection->query($sql);
        while ($row = $query->fetch_assoc()) {
            $result[] = $this->createAndFetch($row);
        }

        return $result;
    }

    private function createAndFetch(array $row)
    {
        $mappedRow = [];
        foreach ($row as $key => $value) {
            $mappedRow[$this->dbMapping[$key]] = $value;
        }

        return $this->orm->createInstance($this->reflection, $mappedRow);
    }

    protected function getManager()
    {
        return $this->orm;
    }
}