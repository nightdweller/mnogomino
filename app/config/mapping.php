<?php

return [
    'App\\Entity\\User' => [
        'repository' => 'App\\Entity\\Repository\\UserRepository',
        'table' => 'users',
        'mapping' => [
            'id' => 'id',
            'vkId' => 'vk_id',
            'level' => 'level',
            'lastUpdate' => 'last_update'
        ]
    ]
];
