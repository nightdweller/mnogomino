<?php
return [
    'request' => ['class' => 'Components\\Request'],
    'router' => [
        'class' => 'Components\\Router',
        'arguments' => ['@container']
    ],
    'db' => [
        'class' => 'Components\\DB\\DB',
        'arguments' => [
            '127.0.0.1',
            'root',
            '16729438',
            'test',
        ]
    ],
    'orm' => [
        'class' => 'Components\\DB\\ObjectManager',
        'arguments' => ['@container']
    ],
    'php_template' => [
        'class' => 'Components\\PhpTemplate\\Template',
        'arguments' => ['@container']
    ],
    'user_token' => ['class' => 'Components\\UserToken']
];