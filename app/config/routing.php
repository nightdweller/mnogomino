<?php

return [
    '/' => [
        'App\\Controller\\GameController',
        'index'
    ],
    '/main' => [
        'App\\Controller\\GameController',
        'main'
    ],
    '/game' => [
        'App\\Controller\\GameController',
        'game'
    ],
    '/rating/city' => [
        'App\\Controller\\RatingController',
        'city'
    ]
];
