$(document).ready(function() {
    function resize($object, left, top, width, height) {
        $object.css({
            top: top + 'px',
            left: left + 'px',
            width: width + 'px',
            height: height
        })
    }

    function gnezorkMsg(html) {
        $('.monster .monster-txt').html(html);
        $('.monster').show('fast');
    }

    function square(x, y, x2, y2) {
        var width = x2 - x + 2;
        var height = y2 - y + 2;

        var $left = $('.area-left');
        var $right = $('.area-right');
        var $top = $('.area-top');
        var $bottom = $('.area-bottom');

        var w = {width: 850, height: 660};

        resize($top, x + 0, 0, width, y + 'px');
        resize($bottom, x + 0, y + height, width, (w.height - y - height) + 'px');

        resize($left, 0, 0, x, '100%');
        resize($right, x + width, 0, w.width - x - width, '100%');

        $left.show('fast');
        $right.show('fast');
        $top.show('fast');
        $bottom.show('fast');
    }

    function hideSquare() {
        var $left = $('.area-left');
        var $right = $('.area-right');
        var $top = $('.area-top');
        var $bottom = $('.area-bottom');

        $left.hide();
        $right.hide();
        $top.hide();
        $bottom.hide();
    }

    function hideGnezork() {
        $('.monster').hide();
    }

    setTimeout(function () {
    }, 1000);

    (function () {
        var jsn = 1;
        var arr = [
            {},
            {msg: '<strong>Привет!</strong><br/> Я Иван. Давай поиграем!', pos: [290, 426, 555, 495]},
            {msg: 'Это окно с уровнями. Жми на кнопку', pos: [273, 385, 571, 443]},
            {msg: 'Нажми на синие квадратики', pos: [200, 200, 650, 493]},
            {msg: 'Похожие квадратики рядышком исчезают', pos: [0, 0, 900, 800]},
            {msg: 'Сделай так, чтобы все квадратики исчезли', pos: [200, 200, 650, 493]}
        ];

        function increment() {
            setTimeout(function () {
                var $js = $('.js-' + jsn);
                var obj = arr[jsn].pos;

                square(obj[0], obj[1], obj[2], obj[3]);
                gnezorkMsg(arr[jsn].msg);

                $(document).on('click', '.js-' + jsn, function () {
                    hideSquare();
                    hideGnezork();
                    $(document).off('click', '.js-' + jsn);
                    jsn++;
                    increment();
                })
            }, 1);
        }

        if (App.isFirst) {
            increment();
            var mhhv = 0;

            function ddd() {
                if (mhhv == 0) {
                    $('.mhl').removeClass('ok');
                    $('.mhr').removeClass('ok');
                    mhhv = 1;
                } else {
                    $('.mhl').addClass('ok');
                    $('.mhr').addClass('ok');
                    mhhv = 0;
                }
            }

            setInterval(ddd, 1500);
            ddd();
        }
    })();
});