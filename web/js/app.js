App = {
    intervals: [],
    appFriendsIds: [],
    appFriends: [],
    friends: []
};

App.page = function (url, bodyclass, data) {
    if (!data) {
        data = {};
    }
    data.viewer_id = User.vkId;

    $.ajax({
        url: url,
        type: 'post',
        data: data
    }).done(function (data) {
        if (onunload) {
            onunload();
        }

        if (bodyclass) {
            $('body').attr('class', bodyclass);
        }

        $('.js-container').html(data);
    });

    App.clear();
};

App.messageCallback = function(){};

App.message = function (head, content, btnvalue, callback) {
    $('.js-yd-msg-header').html(head);
    $('.js-yd-msg-content').html(content);
    $('.js-yd-msg-close').html(btnvalue);

    $('.js-yd-black,.js-yd-msg').show('slow');

    if (callback) {
        App.messageCallback = callback;
    } else {
        App.messageCallback = function(){};
    }
};

App.closeMessage = function () {
    App.messageCallback();
    $('.js-yd-black,.js-yd-msg').hide('slow');
};

App.buy = function (num, callback) {
    VK.callMethod('showOrderBox', {
        type: 'votes',
        votes: num
    });
};

App.api = function (method, params) {
    return $.ajax({
        url: App.apiUrl,
        type: 'post',
        dataType: 'json',
        data: $.extend({method: method}, params)
    });
};

App.vk = function (method, params, callback) {
    if (undefined == callback) {
        return;
    }
    App.callback = callback;
    var script = document.createElement('SCRIPT');

    script.src = 'https://api.vk.com/method/' + method + '?' + $.param($.extend({
        access_token: User.vkToken,
        callback: 'App.callback',
        v: '5.28'
    }, params));

    document.getElementsByTagName("head")[0].appendChild(script);
};

App.callback = function (data) {
    console.log(data);
};

App.setInterval = function (func, time) {
    var interval = setInterval(func, time);
    App.intervals.push(interval);

    return interval;
};

App.onclear = function () {
};

App.clear = function () {
    for (var i = 0; i < App.intervals.length; i++) {
        clearInterval(App.intervals[i]);
    }
    App.intervals = [];

    App.onclear();
};

$(document).ready(function() {
    $('.js-yd-black,.js-yd-msg').hide();
});