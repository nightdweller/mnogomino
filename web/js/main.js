$(document).on('click', 'button[data-href]', function (e) {
    $.ajax({
        url: $(this).attr('data-href')
    }).done(function (data) {
        $('.js-content').html(data);
    });
});