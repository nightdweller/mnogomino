Game = {
    level: 1,
    appId: 1,
    points: 0,
    outPoints: 0,
    messagePage: ''
};

//Game.over = function ()
//{
//    App.
//};

Game.setPoints = function (points) {
    Game.points = points;
};

Game.incPoints = function (val) {
    Game.points = parseInt(Game.points) + val;
    return Game.points;
};

Game.sendPoints = function (points) {
    if (points == undefined) {
        return Api.setPoints(Game.points, Game.level);
    }
    return Api.setPoints(points, Game.level);
};

Game.incLevel = function () {
    Game.level++;
    return Api.setLevel(parseInt(Game.level));
};

Game.getOutPoints = function () {
    return parseInt(Game.outPoints).toString();
};

Game.win = function (callback) {
    Game.sendPoints().done(function () {
        Game.incLevel().done(function () {
            if (callback) {
                callback();
            }
        })
    });
};

Game.over = function () {
    Game.sendPoints().done(function () {
        App.page(Game.messagePage, 'green');
    });
};

setInterval(function () {
    Game.outPoints += Math.abs(Game.points - Game.outPoints) / 5;

    if (Math.abs(Game.outPoints - Game.points) < 0.5) {
        Game.outPoints = Game.points;
    }
}, 30);

Api = {};

Api.getVkUsers = function (ids) {
    return App.api('getvkusers', {
        ids: ids
    });
};

Api.setPoints = function (points, level) {
    return App.api('setpoints', {
        points: points,
        level: level
    });
};

Api.setLevel = function (level) {
    return App.api('setlevel', {
        level: level
    });
};

Api.getRating = function (level, vkIds, group) {
    var data = {level: level};

    if (vkIds) {
        data.vkIds = vkIds;
    }

    data.group = undefined == group ? 1 : (group ? 1 : 0);

    return App.api('getpoints', data);
};