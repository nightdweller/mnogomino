<?php

$time = microtime(true);

ini_set('display_errors', 1);

const WEB_DIR = __DIR__;
$loader = require_once __DIR__ . '/../vendor/autoload.php';

use Components\Kernel\Kernel;
use App\EventListener\EventListener;
$kernel = new Kernel('App\\Container\\Container');

EventListener::preHandle($kernel->getContainer());
//
$response = $kernel->handleRequest();
//
EventListener::postHandle($kernel->getContainer());
//
$response->send();
//
echo('<br />');
var_dump((microtime(true) - $time) * 1000);